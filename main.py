import discord
import json
import os
import dotenv
import requests
import random
from datetime import datetime
from database import otazky
from keep_alive import keep_alive
from discord.ext.commands import MissingPermissions
from discord.ext import commands
from dotenv import load_dotenv
from bs4 import BeautifulSoup
import unidecode
papousek = ["super", "uzasny", "papousek"]


client = discord.Client()
client = commands.Bot(command_prefix=",")
client.remove_command("help")
load_dotenv()
TOKEN = os.getenv('TOKEN')



@client.event  
async def on_ready():
    print("-------------------------")
    print("Bot Name: " + client.user.name)
    print(client.user.id)
    print("API Version: " + discord.__version__)
    print(client.latency * 1000)
    print("-------------------------")

    await client.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name="odpocet pro auto"))
    
    @commands.Cog.listener()
    async def on_ready(self):
        print("Main fachčí - ✔️ ")


@client.event
async def on_reaction_add(reaction, user):
    print(reaction)

#---------------------------------#
#            verify               #
#---------------------------------#

@client.command()
@commands.has_role("new")
async def verify(ctx): 
    await ctx.channel.purge(limit=1)
    if ctx.channel.id != 791623298710372354:
        return 
    else:
        otazkabez = random.choice(otazky)
        odpoved = otazkabez[-1]
        otazkabez = otazkabez.replace(otazkabez[-1], " ")
        emb=discord.Embed(title='Vstupní test',description='Pro odpověď zareaguj jedním z těchto emotů kdy: \n✅ = Ano \n❌ = Ne \nDoporučuji si dobře rozmyslet svoji odpověďď, máš jen jeden pokus a pokud zareaguješ špatně, bude to bráno jako špatná odpověď.', colour=0xffff00)
        emb.add_field(name="Otázka:", value=f"{otazkabez}")
        mess = await ctx.author.send(embed=emb)
        role = discord.utils.get(ctx.guild.roles, name="new")
        user = ctx.message.author
        await user.remove_roles(role)
        moznosti = ["✅", "❌"]
        for uzodp in moznosti:
            await mess.add_reaction(uzodp)
        def check1(reaction, user):
            return user != client.user and str(reaction.emoji)

        reaction, user = await client.wait_for("reaction_add", timeout=30.0, check=check1)
        
        if str(reaction.emoji) == odpoved:
                role = discord.utils.get(ctx.guild.roles, name="▼ MEMBER")
                user = ctx.message.author
                await user.add_roles(role)
                await ctx.author.send("Vítej na serveru OMSFANS :)")
        else: 
            await ctx.author.send("Bohužel špatná odpověď, pokud si myslíš, že je chyba, napiš mi na ig nebo na git. \nIg: @ten.majkl / @holubkuba \nGitLab  repozitář: https://gitlab.com/TENMAJKL/ctenar/")
            neprosel = ctx.message.author
            await neprosel.kick(reason="Špatná odpověď")
        #log
        ted = str(datetime.now()) 
        log_message= ted + " | "+ctx.message.author.name + " Aktivoval příkaz ,verify asi se pokousi ziskat membera"
        print(log_message, file=open('log.txt', 'a'))
#---------------------------------#
#            verify               #
#---------------------------------#

#clear

@client.command()
@commands.has_any_role("▼ OMSF CREW", "▼ MODERATOR")
async def clear(ctx, amount=2):
    await ctx.channel.purge(limit = amount )

@client.event
async def on_message(message):
    if message.attachments:
        obrazek = message.attachments[0].url
        ted = str(datetime.now()) 
        log_message= ted + " | "+ message.author.name + " Poslal tento soubor: " + obrazek
        print(log_message, file=open('log.txt', 'a'))  
    

    if "vim" in message.content.lower():
        emoji = discord.utils.get(client.emojis, name='vim')
        await message.add_reaction(emoji)
    for i in papousek:
        if i in unidecode.unidecode(message.content.lower()):
            emoji = discord.utils.get(client.emojis, name='uzasny')
            await message.add_reaction(emoji)
    await client.process_commands(message)


@client.command()
async def pridat(ctx, odkaz):
    if ctx.channel.id != 781204852417250: 
        return 
    else:   
        user = "Majkel#4908"
        await user.create_dm()
        await user.dm_channel.send(odkaz)



@client.command()
async def knp(ctx):
    if ctx.channel.id != 774284623193702420:
        return 
    else:
        status = ''
        await ctx.message.add_reaction("✅")
        emb=discord.Embed(title='Kámen nůžky papír teď',description='Hraješ proti Čtenářovi\nZareaguj pomocí těchto emoji:\n⛰-Kámen\n✂️-Nůžky\n📄-Papír', colour=0xffff00)
        message = await ctx.author.send(embed=emb) 
        reakce = ["⛰","✂️","📄"]
        for emoji in reakce:
            await message.add_reaction(emoji)
        def check1(reaction, user):
            return user != client.user and str(reaction.emoji)

        reaction, user = await client.wait_for("reaction_add", timeout=30.0, check=check1)
        moznosti = ["⛰","✂️","📄"]
        metmoz = random.choice(moznosti)
        if str(reaction.emoji) == metmoz:
            emb=discord.Embed(title='Kámen nůžky papír teď',description='Hraješ proti Čtenářovi\nRezmíza!', colour=0xffff00)  
            emb.add_field(name="Moje odpověď", value=f"{metmoz}") 
            emb.add_field(name="Tvoje odpověď", value=f"{reaction.emoji}") 
            await ctx.author.send(embed=emb) 
            status = "remiza"
        elif str(reaction.emoji) == "⛰" and metmoz == "✂️":
            emb=discord.Embed(title='Kámen nůžky papír teď',description='Hraješ proti Čtenářovi\nVyhrál jsi!', colour=0xffff00)  
            emb.add_field(name="Moje odpověď", value=f"{metmoz}") 
            emb.add_field(name="Tvoje odpověď", value=f"{reaction.emoji}") 
            await ctx.author.send(embed=emb) 
            status = "vyhral"
        elif str(reaction.emoji) == "✂️" and metmoz == "⛰":
            emb=discord.Embed(title='Kámen nůžky papír teď',description='Hraješ proti Čtenářovi\nVyhrál jsem!', colour=0xffff00)  
            emb.add_field(name="Moje odpověď", value=f"{metmoz}") 
            emb.add_field(name="Tvoje odpověď", value=f"{reaction.emoji}") 
            await ctx.author.send(embed=emb) 
            status = "prohral"
        elif str(reaction.emoji) == "📄" and metmoz == "✂️":
            emb=discord.Embed(title='Kámen nůžky papír teď',description='Hraješ proti Čtenářovi\nVyhrál jsem!', colour=0xffff00)  
            emb.add_field(name="Moje odpověď", value=f"{metmoz}") 
            emb.add_field(name="Tvoje odpověď", value=f"{reaction.emoji}")
            await ctx.author.send(embed=emb) 
            status = "prohral"
        elif str(reaction.emoji) == "✂️" and metmoz == "📄":
            emb=discord.Embed(title='Kámen nůžky papír teď',description='Hraješ proti Čtenářovi\nVyhrál jsi!', colour=0xffff00)  
            emb.add_field(name="Moje odpověď", value=f"{metmoz}") 
            emb.add_field(name="Tvoje odpověď", value=f"{reaction.emoji}") 
            await ctx.author.send(embed=emb) 
            status = "vyhral"
        elif str(reaction.emoji) == "📄" and metmoz == "⛰":
            emb=discord.Embed(title='Kámen nůžky papír teď',description='Hraješ proti Čtenářovi\nVyhrál jsi!', colour=0xffff00)  
            emb.add_field(name="Moje odpověď", value=f"{metmoz}") 
            emb.add_field(name="Tvoje odpověď", value=f"{reaction.emoji}") 
            await ctx.author.send(embed=emb) 
            status = "vyhral"
        elif str(reaction.emoji) == "⛰" and metmoz == "📄":
            emb=discord.Embed(title='Kámen nůžky papír teď',description='Hraješ proti Čtenářovi\nVyhrál jsem!', colour=0xffff00)  
            emb.add_field(name="Moje odpověď", value=f"{metmoz}") 
            emb.add_field(name="Tvoje odpověď", value=f"{reaction.emoji}")
            await ctx.author.send(embed=emb) 
            status = "prohral"
      #log
        ted = str(datetime.now()) 
        log_message= ted + " | "+ctx.message.author.name + " Aktivoval příkaz ,knp a " + status
        print(log_message, file=open('log.txt', 'a'))


keep_alive()
client.run(TOKEN)

